# French translation for gnome-shell-extension-openweather
# Traductions françaises du paquet gnome-shell-extension-openweather.
# Copyright (C) 2011
# This file is distributed under the same license as the gnome-shell-extension-weather package.
# Simon Claessens <gagalago@gmail.com>, 2011.
# ecyrbe <ecyrbe@gmail.com>, 2011.
# Davy Defaud (DevDef) <davy.defaud@free.fr>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: 3.0.1\n"
"Report-Msgid-Bugs-To: https://gitlab.com/jenslody/gnome-shell-extension-"
"openweather/issues\n"
"POT-Creation-Date: 2021-05-09 10:46+0200\n"
"PO-Revision-Date: 2017-02-04 20:26+0100\n"
"Last-Translator: Davy Defaud (DevDef) <davy.defaud@free.fr>\n"
"Language-Team: Français <ecyrbe@gmail.com>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n>=2);\n"
"X-Generator: Gtranslator 2.91.7\n"

#: src/extension.js:181
msgid "..."
msgstr "…"

#: src/extension.js:360
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""
"Openweathermap.org ne fonctionne pas sans une clef d’API.\n"
"Veuillez soit activer le commutateur permettant de choisir la clef des "
"extensions par défaut dans les préférences, soit vous enregistrer sur http://"
"openweathermap.org/appid et coller votre clef personnelle dans lesdites "
"préférences."

#: src/extension.js:414
msgid ""
"Dark Sky does not work without an api-key.\n"
"Please register at https://darksky.net/dev/register and paste your personal "
"key into the preferences dialog."
msgstr ""
"Dark Sky ne fonctionne pas sans une clef d’API.\n"
"Veuillez vous enregistrer sur https://darksky.net/dev/register et préciser "
"votre clef personnelle dans les préférences."

#: src/extension.js:519 src/extension.js:531
#, javascript-format
msgid "Can not connect to %s"
msgstr "Impossible d’ouvrir %s"

#: src/extension.js:843 data/weather-settings.ui:300
msgid "Locations"
msgstr "Emplacements"

#: src/extension.js:855
msgid "Reload Weather Information"
msgstr "Recharger les informations météo"

#: src/extension.js:870
msgid "Weather data provided by:"
msgstr "Données météo fournies par :"

#: src/extension.js:880
#, javascript-format
msgid "Can not open %s"
msgstr "Impossible d’ouvrir %s"

#: src/extension.js:887
msgid "Weather Settings"
msgstr "Paramètres de la météo"

#: src/extension.js:938 src/prefs.js:1059
msgid "Invalid city"
msgstr "Ville incorrecte"

#: src/extension.js:949
msgid "Invalid location! Please try to recreate it."
msgstr "Localisation invalide ! Veuillez essayer de la recréer."

#: src/extension.js:1000 data/weather-settings.ui:567
msgid "°F"
msgstr "°F"

#: src/extension.js:1002 data/weather-settings.ui:568
msgid "K"
msgstr "K"

#: src/extension.js:1004 data/weather-settings.ui:569
msgid "°Ra"
msgstr "°Ra"

#: src/extension.js:1006 data/weather-settings.ui:570
msgid "°Ré"
msgstr "°Ré"

#: src/extension.js:1008 data/weather-settings.ui:571
msgid "°Rø"
msgstr "°Rø"

#: src/extension.js:1010 data/weather-settings.ui:572
msgid "°De"
msgstr "°De"

#: src/extension.js:1012 data/weather-settings.ui:573
msgid "°N"
msgstr "°N"

#: src/extension.js:1014 data/weather-settings.ui:566
msgid "°C"
msgstr "°C"

#: src/extension.js:1055
msgid "Calm"
msgstr "Calme"

#: src/extension.js:1058
msgid "Light air"
msgstr "Très légère brise"

#: src/extension.js:1061
msgid "Light breeze"
msgstr "Légère brise"

#: src/extension.js:1064
msgid "Gentle breeze"
msgstr "Petite brise"

#: src/extension.js:1067
msgid "Moderate breeze"
msgstr "Brise modérée"

#: src/extension.js:1070
msgid "Fresh breeze"
msgstr "Brise fraîche"

#: src/extension.js:1073
msgid "Strong breeze"
msgstr "Forte brise"

#: src/extension.js:1076
msgid "Moderate gale"
msgstr "Coup de vent"

#: src/extension.js:1079
msgid "Fresh gale"
msgstr "Coup de vent frais"

#: src/extension.js:1082
msgid "Strong gale"
msgstr "Fort coup de vent"

#: src/extension.js:1085
msgid "Storm"
msgstr "Tempête"

#: src/extension.js:1088
msgid "Violent storm"
msgstr "Violente tempête"

#: src/extension.js:1091
msgid "Hurricane"
msgstr "Ouragan"

#: src/extension.js:1095
msgid "Sunday"
msgstr "dimanche"

#: src/extension.js:1095
msgid "Monday"
msgstr "lundi"

#: src/extension.js:1095
msgid "Tuesday"
msgstr "mardi"

#: src/extension.js:1095
msgid "Wednesday"
msgstr "mercredi"

#: src/extension.js:1095
msgid "Thursday"
msgstr "jeudi"

#: src/extension.js:1095
msgid "Friday"
msgstr "vendredi"

#: src/extension.js:1095
msgid "Saturday"
msgstr "samedi"

#: src/extension.js:1101
msgid "N"
msgstr "N"

#: src/extension.js:1101
msgid "NE"
msgstr "NE"

#: src/extension.js:1101
msgid "E"
msgstr "E"

#: src/extension.js:1101
msgid "SE"
msgstr "SE"

#: src/extension.js:1101
msgid "S"
msgstr "S"

#: src/extension.js:1101
msgid "SW"
msgstr "SO"

#: src/extension.js:1101
msgid "W"
msgstr "O"

#: src/extension.js:1101
msgid "NW"
msgstr "NO"

#: src/extension.js:1174 src/extension.js:1183 data/weather-settings.ui:600
msgid "hPa"
msgstr "hPa"

#: src/extension.js:1178 data/weather-settings.ui:601
msgid "inHg"
msgstr "inHg"

#: src/extension.js:1188 data/weather-settings.ui:602
msgid "bar"
msgstr "bar"

#: src/extension.js:1193 data/weather-settings.ui:603
msgid "Pa"
msgstr "Pa"

#: src/extension.js:1198 data/weather-settings.ui:604
msgid "kPa"
msgstr "kPa"

#: src/extension.js:1203 data/weather-settings.ui:605
msgid "atm"
msgstr "atm"

#: src/extension.js:1208 data/weather-settings.ui:606
msgid "at"
msgstr "at"

#: src/extension.js:1213 data/weather-settings.ui:607
msgid "Torr"
msgstr "Torr"

#: src/extension.js:1218 data/weather-settings.ui:608
msgid "psi"
msgstr "psi"

#: src/extension.js:1223 data/weather-settings.ui:609
msgid "mmHg"
msgstr "mmHg"

#: src/extension.js:1228 data/weather-settings.ui:610
msgid "mbar"
msgstr "mbar"

#: src/extension.js:1272 data/weather-settings.ui:586
msgid "m/s"
msgstr "m/s"

#: src/extension.js:1276 data/weather-settings.ui:585
msgid "mph"
msgstr "mph"

#: src/extension.js:1281 data/weather-settings.ui:584
msgid "km/h"
msgstr "km/h"

#: src/extension.js:1290 data/weather-settings.ui:587
msgid "kn"
msgstr "nd"

#: src/extension.js:1295 data/weather-settings.ui:588
msgid "ft/s"
msgstr "ft/s"

#: src/extension.js:1389
msgid "Loading ..."
msgstr "Chargement…"

#: src/extension.js:1393
msgid "Please wait"
msgstr "Veuillez patienter"

#: src/extension.js:1454
msgid "Cloudiness:"
msgstr "Nébulosité :"

#: src/extension.js:1458
msgid "Humidity:"
msgstr "Humidité :"

#: src/extension.js:1462
msgid "Pressure:"
msgstr "Pression :"

#: src/extension.js:1466
msgid "Wind:"
msgstr "Vent :"

#: src/darksky_net.js:159 src/darksky_net.js:291 src/openweathermap_org.js:352
#: src/openweathermap_org.js:479
msgid "Yesterday"
msgstr "Hier"

#: src/darksky_net.js:162 src/darksky_net.js:294 src/openweathermap_org.js:354
#: src/openweathermap_org.js:481
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "Il y a %d jour"
msgstr[1] "Il y a %d jours"

#: src/darksky_net.js:174 src/darksky_net.js:176 src/openweathermap_org.js:368
#: src/openweathermap_org.js:370
msgid ", "
msgstr ""

#: src/darksky_net.js:271 src/openweathermap_org.js:473
msgid "Today"
msgstr "Aujourd’hui"

#: src/darksky_net.js:287 src/openweathermap_org.js:475
msgid "Tomorrow"
msgstr "Demain"

#: src/darksky_net.js:289 src/openweathermap_org.js:477
#, javascript-format
msgid "In %d day"
msgid_plural "In %d days"
msgstr[0] "Dans %d jour"
msgstr[1] "Dans %d jours"

#: src/openweathermap_org.js:183
msgid "Thunderstorm with light rain"
msgstr "Orage avec pluie légère"

#: src/openweathermap_org.js:185
msgid "Thunderstorm with rain"
msgstr "Orage avec pluie"

#: src/openweathermap_org.js:187
msgid "Thunderstorm with heavy rain"
msgstr "Orage avec de fortes pluies"

#: src/openweathermap_org.js:189
msgid "Light thunderstorm"
msgstr "Orages isolés"

#: src/openweathermap_org.js:191
msgid "Thunderstorm"
msgstr "Orage"

#: src/openweathermap_org.js:193
msgid "Heavy thunderstorm"
msgstr "Orage sévère"

#: src/openweathermap_org.js:195
msgid "Ragged thunderstorm"
msgstr "Orages isolés"

#: src/openweathermap_org.js:197
msgid "Thunderstorm with light drizzle"
msgstr "Orage avec bruine légère"

#: src/openweathermap_org.js:199
msgid "Thunderstorm with drizzle"
msgstr "Orage avec bruine"

#: src/openweathermap_org.js:201
msgid "Thunderstorm with heavy drizzle"
msgstr "Orage avec forte bruine"

#: src/openweathermap_org.js:203
msgid "Light intensity drizzle"
msgstr "Bruine légère"

#: src/openweathermap_org.js:205
msgid "Drizzle"
msgstr "Bruine"

#: src/openweathermap_org.js:207
msgid "Heavy intensity drizzle"
msgstr "Bruine forte"

#: src/openweathermap_org.js:209
msgid "Light intensity drizzle rain"
msgstr "Bruine légère"

#: src/openweathermap_org.js:211
msgid "Drizzle rain"
msgstr "Bruine"

#: src/openweathermap_org.js:213
msgid "Heavy intensity drizzle rain"
msgstr "Pluie forte"

#: src/openweathermap_org.js:215
msgid "Shower rain and drizzle"
msgstr "Averses"

#: src/openweathermap_org.js:217
msgid "Heavy shower rain and drizzle"
msgstr "Fortes averses"

#: src/openweathermap_org.js:219
msgid "Shower drizzle"
msgstr "Bruine"

#: src/openweathermap_org.js:221
msgid "Light rain"
msgstr "Légère pluie"

#: src/openweathermap_org.js:223
msgid "Moderate rain"
msgstr "Pluie modérée"

#: src/openweathermap_org.js:225
msgid "Heavy intensity rain"
msgstr "Pluie forte"

#: src/openweathermap_org.js:227
msgid "Very heavy rain"
msgstr "Pluie très forte"

#: src/openweathermap_org.js:229
msgid "Extreme rain"
msgstr "Pluie extrêmement forte"

#: src/openweathermap_org.js:231
msgid "Freezing rain"
msgstr "Pluie verglaçante"

#: src/openweathermap_org.js:233
msgid "Light intensity shower rain"
msgstr "Averses légères"

#: src/openweathermap_org.js:235
msgid "Shower rain"
msgstr "Averses"

#: src/openweathermap_org.js:237
msgid "Heavy intensity shower rain"
msgstr "Fortes averses"

#: src/openweathermap_org.js:239
msgid "Ragged shower rain"
msgstr "Averses isolées"

#: src/openweathermap_org.js:241
msgid "Light snow"
msgstr "Faibles chutes de neige"

#: src/openweathermap_org.js:243
msgid "Snow"
msgstr "Neige"

#: src/openweathermap_org.js:245
msgid "Heavy snow"
msgstr "Fortes chutes de neige"

#: src/openweathermap_org.js:247
msgid "Sleet"
msgstr "Grésil"

#: src/openweathermap_org.js:249
msgid "Shower sleet"
msgstr "Grésil"

#: src/openweathermap_org.js:251
msgid "Light rain and snow"
msgstr "Neige et pluie"

#: src/openweathermap_org.js:253
msgid "Rain and snow"
msgstr "Neige et pluie"

#: src/openweathermap_org.js:255
msgid "Light shower snow"
msgstr "Faibles averses de neige"

#: src/openweathermap_org.js:257
msgid "Shower snow"
msgstr "Averses"

#: src/openweathermap_org.js:259
msgid "Heavy shower snow"
msgstr "Fortes chutes de neige"

#: src/openweathermap_org.js:261
msgid "Mist"
msgstr "Brouillard"

#: src/openweathermap_org.js:263
msgid "Smoke"
msgstr "Brume"

#: src/openweathermap_org.js:265
msgid "Haze"
msgstr "Brume"

#: src/openweathermap_org.js:267
msgid "Sand/Dust Whirls"
msgstr "Tourbillons de sable/poussière"

#: src/openweathermap_org.js:269
msgid "Fog"
msgstr "Brouillard"

#: src/openweathermap_org.js:271
msgid "Sand"
msgstr "Sable"

#: src/openweathermap_org.js:273
msgid "Dust"
msgstr "Poussière"

#: src/openweathermap_org.js:275
msgid "VOLCANIC ASH"
msgstr "CENDRES VOLCANIQUES"

#: src/openweathermap_org.js:277
msgid "SQUALLS"
msgstr "ORAGES"

#: src/openweathermap_org.js:279
msgid "TORNADO"
msgstr "TORNADE"

#: src/openweathermap_org.js:281
msgid "Sky is clear"
msgstr "Ciel dégagé"

#: src/openweathermap_org.js:283
msgid "Few clouds"
msgstr "Quelques nuages"

#: src/openweathermap_org.js:285
msgid "Scattered clouds"
msgstr "Nuages épars"

#: src/openweathermap_org.js:287
msgid "Broken clouds"
msgstr "Nuages fragmentés"

#: src/openweathermap_org.js:289
msgid "Overcast clouds"
msgstr "Couvert"

#: src/openweathermap_org.js:291
msgid "Not available"
msgstr "Non disponible"

#: src/openweathermap_org.js:384
msgid "?"
msgstr ""

#: src/prefs.js:197
#, fuzzy
msgid "Searching ..."
msgstr "Chargement…"

#: src/prefs.js:209 src/prefs.js:247 src/prefs.js:278 src/prefs.js:282
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr "Données invalides pour « %s »"

#: src/prefs.js:214 src/prefs.js:254 src/prefs.js:285
#, javascript-format
msgid "\"%s\" not found"
msgstr "Schéma « %s » manquant"

#: src/prefs.js:232
#, fuzzy
msgid "You need an AppKey to search on openmapquest."
msgstr "Votre clef d’API personnelle de developer.mapquest.com"

#: src/prefs.js:233
#, fuzzy
msgid "Please visit https://developer.mapquest.com/ ."
msgstr "Clef d’API personnelle de developer.mapquest.com"

#: src/prefs.js:248
msgid "Do you use a valid AppKey to search on openmapquest ?"
msgstr ""

#: src/prefs.js:249
msgid "If not, please visit https://developer.mapquest.com/ ."
msgstr ""

#: src/prefs.js:339
msgid "Location"
msgstr "Emplacement"

#: src/prefs.js:350
msgid "Provider"
msgstr "Fournisseur"

#: src/prefs.js:360
msgid "Result"
msgstr ""

#: src/prefs.js:550
#, javascript-format
msgid "Remove %s ?"
msgstr "Supprimer %s ?"

#: src/prefs.js:563
#, fuzzy
msgid "No"
msgstr "N"

#: src/prefs.js:564
msgid "Yes"
msgstr ""

#: src/prefs.js:1094
msgid "default"
msgstr "par défaut"

#: data/weather-settings.ui:25
msgid "Edit name"
msgstr "Éditer le nom"

#: data/weather-settings.ui:36 data/weather-settings.ui:52
#: data/weather-settings.ui:178
msgid "Clear entry"
msgstr "Dégagé"

#: data/weather-settings.ui:43
msgid "Edit coordinates"
msgstr "Éditer les coordonnées"

#: data/weather-settings.ui:59 data/weather-settings.ui:195
msgid "Extensions default weather provider"
msgstr "Fournisseur météo par défaut des extensions"

#: data/weather-settings.ui:78 data/weather-settings.ui:214
msgid "Cancel"
msgstr "Annuler"

#: data/weather-settings.ui:88 data/weather-settings.ui:224
msgid "Save"
msgstr "Sauvegarder"

#: data/weather-settings.ui:164
msgid "Search by location or coordinates"
msgstr "Chercher par lieu ou coordonnées"

#: data/weather-settings.ui:179
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "ex : Paris, France ou 48.8534100,2.3488000"

#: data/weather-settings.ui:184
msgid "Find"
msgstr "Trouver"

#: data/weather-settings.ui:318
msgid "Chose default weather provider"
msgstr "Choisir le fournisseur météo par défaut"

#: data/weather-settings.ui:329
msgid "Personal Api key from openweathermap.org"
msgstr "Clef d’API personnelle d’openweathermap.org"

#: data/weather-settings.ui:372
msgid "Personal Api key from Dark Sky"
msgstr "Clef d’API personnelle de Dark Sky"

#: data/weather-settings.ui:383
msgid "Refresh timeout for current weather [min]"
msgstr "Délai avant rafraîchissement de l’observation météo [min]"

#: data/weather-settings.ui:395
msgid "Refresh timeout for weather forecast [min]"
msgstr "Délai avant rafraîchissement des prévisions météo [min]"

#: data/weather-settings.ui:418
msgid ""
"Note: the forecast-timout is not used for Dark Sky, because they do not "
"provide seperate downloads for current weather and forecasts."
msgstr ""
"Note : le délai de rafraîchissement des prévisions n’est pas utilisé pour "
"Dark Sky, parce que ce dernier ne fournit pas de flux séparés entre les "
"observations et les prévisions."

#: data/weather-settings.ui:441
msgid "Use extensions api-key for openweathermap.org"
msgstr "Utiliser la clef d’API d’extension pour openweathermap.org"

#: data/weather-settings.ui:450
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""
"Désactivez, si vous avez votre propre clef d’API pour openweathermap.org que "
"vous copierez dans le champ de saisie ci‐dessous."

#: data/weather-settings.ui:462
msgid "Weather provider"
msgstr "Fournisseur météo"

#: data/weather-settings.ui:478
msgid "Chose geolocation provider"
msgstr "Choisissez le fournisseur de géolocalisation"

#: data/weather-settings.ui:500
msgid "Personal AppKey from developer.mapquest.com"
msgstr "Clef d’API personnelle de developer.mapquest.com"

#: data/weather-settings.ui:522
msgid "Geolocation provider"
msgstr "Fournisseur de géolocalisation"

#: data/weather-settings.ui:538
#: data/org.gnome.shell.extensions.openweather.gschema.xml:63
msgid "Temperature Unit"
msgstr "Unité de température"

#: data/weather-settings.ui:547
msgid "Wind Speed Unit"
msgstr "Unité de vitesse du vent"

#: data/weather-settings.ui:556
#: data/org.gnome.shell.extensions.openweather.gschema.xml:67
msgid "Pressure Unit"
msgstr "Unité de pression"

#: data/weather-settings.ui:589
msgid "Beaufort"
msgstr "Beaufort"

#: data/weather-settings.ui:622
msgid "Units"
msgstr "Unités de mesure"

#: data/weather-settings.ui:638
#: data/org.gnome.shell.extensions.openweather.gschema.xml:113
msgid "Position in Panel"
msgstr "Position sur le tableau de bord"

#: data/weather-settings.ui:647
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr "Position de la boîte de menu [en %] de 0 (gauche) à 100 (droite)"

#: data/weather-settings.ui:656
#: data/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Wind Direction by Arrows"
msgstr "Direction du vent avec des flèches"

#: data/weather-settings.ui:665
#: data/org.gnome.shell.extensions.openweather.gschema.xml:89
msgid "Translate Conditions"
msgstr "Traduction des conditions"

#: data/weather-settings.ui:674
#: data/org.gnome.shell.extensions.openweather.gschema.xml:93
msgid "Symbolic Icons"
msgstr "Icônes symboliques"

#: data/weather-settings.ui:683
msgid "Text on buttons"
msgstr "Texte sur les boutons"

#: data/weather-settings.ui:692
#: data/org.gnome.shell.extensions.openweather.gschema.xml:101
msgid "Temperature in Panel"
msgstr "Température sur le tableau de bord"

#: data/weather-settings.ui:701
#: data/org.gnome.shell.extensions.openweather.gschema.xml:105
msgid "Conditions in Panel"
msgstr "Conditions sur le tableau de bord"

#: data/weather-settings.ui:710
#: data/org.gnome.shell.extensions.openweather.gschema.xml:109
msgid "Conditions in Forecast"
msgstr "Conditions météo dans les prévisions"

#: data/weather-settings.ui:719
msgid "Center forecast"
msgstr "Centrer la prévision"

#: data/weather-settings.ui:728
#: data/org.gnome.shell.extensions.openweather.gschema.xml:137
msgid "Number of days in forecast"
msgstr "Nombre de jours de prévisions"

#: data/weather-settings.ui:737
#: data/org.gnome.shell.extensions.openweather.gschema.xml:141
msgid "Maximal number of digits after the decimal point"
msgstr "Nombre de décimales"

#: data/weather-settings.ui:747
msgid "Center"
msgstr "au centre"

#: data/weather-settings.ui:748
msgid "Right"
msgstr "à droite"

#: data/weather-settings.ui:749
msgid "Left"
msgstr "à gauche"

#: data/weather-settings.ui:880
#: data/org.gnome.shell.extensions.openweather.gschema.xml:125
msgid "Maximal length of the location text"
msgstr ""

#: data/weather-settings.ui:902
msgid "Layout"
msgstr "Disposition de l’affichage"

#: data/weather-settings.ui:935
msgid "Version: "
msgstr "Version : "

#: data/weather-settings.ui:941
msgid "unknown (self-build ?)"
msgstr "inconnu (auto‐compilation ?)"

#: data/weather-settings.ui:949
msgid ""
"<span>Weather extension to display weather information from <a href="
"\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
"darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
msgstr ""
"<span>Extension GNOME Shell permettant l’affichage des informations météo "
"d’<a href=\"https://openweathermap.org/\">Openweathermap</a> ou <a href="
"\"https://darksky.net\">Dark Sky</a>\n"
" pour la plupart des localisations dans le monde.</span>"

#: data/weather-settings.ui:963
msgid "Maintained by"
msgstr "Maintenu par"

#: data/weather-settings.ui:976
msgid "Webpage"
msgstr "Page Web"

#: data/weather-settings.ui:986
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
"<span size=\"small\">Ce programme est fourni SANS AUCUNE GARANTIE.\n"
"Voir la <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html"
"\">Licence publique générale GNU, versions 2 et supérieures</a>, pour plus "
"de détails.</span>"

#: data/weather-settings.ui:997
msgid "About"
msgstr "À propos"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:55
msgid "Weather Provider"
msgstr "Fournisseur météo"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:59
msgid "Geolocation Provider"
msgstr "Fournisseur de géolocalisation"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid "Wind Speed Units"
msgstr "Unité de vitesse du vent"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:72
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""
"Veuillez choisir l’unité de mesure de la vitesse du vent. Les unités "
"possibles sont : « km/h », « mph »,« m/s »,« nd »,« ft/s » et « Beaufort »."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:77
msgid "Choose whether to display wind direction through arrows or letters."
msgstr ""
"Veuillez choisir si vous préférez afficher la direction du vent par des "
"flèches ou des lettres."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:81
msgid "City to be displayed"
msgstr "Ville à afficher"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:85
msgid "Actual City"
msgstr "Localisation actuelle"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:97
msgid "Use text on buttons in menu"
msgstr "Utiliser du texte dans les boutons de menu"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:117
msgid "Horizontal position of menu-box."
msgstr "Position horizontale de la boîte de menu."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:121
msgid "Refresh interval (actual weather)"
msgstr "Délai de rafraîchissement (observations)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:129
msgid "Refresh interval (forecast)"
msgstr "Délai de rafraîchissement (prévisions)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:133
msgid "Center forecastbox."
msgstr "Centrer le bloc de prévisions"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:145
msgid "Your personal API key from openweathermap.org"
msgstr "Votre clef d’API personnelle d’openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:149
msgid "Use the extensions default API key from openweathermap.org"
msgstr "Utiliser la clef d’API d’extension pour openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:153
msgid "Your personal API key from Dark Sky"
msgstr "Votre clef d’API personnelle de Dark Sky"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:157
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "Votre clef d’API personnelle de developer.mapquest.com"
